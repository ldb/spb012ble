spb012ble
=========

The spb012ble, better known as `Voltcraft SEM6000 SE`_, is a “smart
meter” plug with the following features:

* Remote control via Bluetooth and Android_/`iOS app`_
* Energy measurement with hourly (24), daily (30), monthly (12) statistics
  plus current power, voltage, current and frequency.
* Scheduler with 12 slots
* Timer
* Random mode (anti-burglar)

It looks very similar to the `revogi Smart Meter Plug EU`_ and may be a
rebranded version.

.. _Voltcraft SEM6000 SE: https://www.conrad.de/de/p/voltcraft-sem6000-se-energiekosten-messgeraet-bluetooth-schnittstelle-datenexport-datenloggerfunktion-trms-stromtari-2467903.html
.. _Android: https://play.google.com/store/apps/details?id=com.cei.meter
.. _iOS app: https://apps.apple.com/de/app/voltcraft-sem6000/id1280855960
.. _revogi Smart Meter Plug EU: https://www.revogi.com/smart-power/smart-meter-plug-eu-2/

`This repository`_ contains documentation on the hardware and firmware,
as well as software to control the “smart meter”.

.. _This repository: https://codeberg.org/ldb/spb012ble

Software
--------

To install the software, clone this repository:

.. code-block:: bash

	git clone https://codeberg.org/ldb/spb012ble.git
	virtualenv -p python3 sandbox
	source sandbox/bin/activate
	pip install .

Controlling the device works like this:

.. code-block:: bash

	spb012ble-monitor connect:A3:00:00:00:08:16 auth:0000 sync \
			switch:on led:on measure

Hardware
--------

The device is marked as model no. SEM 6000 40th, but sold as Voltcraft
SEM6000 *SE*. On the bottom in the hole for the type E plug a screw can
be removed. This allows removing the cover of the female socket on the
opposite side.

.. figure:: pictures/_DSC7404_0.small.jpg
   :target: pictures/_DSC7404_0.jpg

This reveals a transparent plastic ring, which is slightly glued. It
can be removed with a guitar pick or thin plastic piece.

.. figure:: pictures/_DSC7405_0.small.jpg
   :target: pictures/_DSC7405_0.jpg

On the bottom of the device there are a few capacitors and resistors,
as well as four RGB-LED’s for the light ring.

.. figure:: pictures/_DSC7407_0.small.jpg
   :target: pictures/_DSC7407_0.jpg

The top half only carries a crystal osciallator.

.. figure:: pictures/_DSC7408_0.small.jpg
   :target: pictures/_DSC7408_0.jpg

The microcontroller PCB (green), which is connected to a flexible
PCB (black), connects to the left side of the main PCB (white).
The microcontroller board is marked with BK3432 V001.  The flexible PCB
has the marking ``SPB012-BLE V0.05 RW, 2021.08.31``

.. figure:: pictures/_DSC7409_0.small.jpg
   :target: pictures/_DSC7409_0.jpg

The Bluetooth chip is marked as `Beken BK3432 EU024JFE
<http://www.bekencorp.com/en/goods/detail/cid/17.html>`__, an ARM968E_
microcontroller with built-in Bluetooth 4.2 or 5.0 [#blversion]_
transceiver. The antenna is to the left.

.. figure:: pictures/_DSC7412_0.small.jpg
   :target: pictures/_DSC7412_0.jpg

.. _ARM968E: https://developer.arm.com/documentation/ddi0311/d/?lang=en
.. [#blversion] Datasheet and website disagree.

The remaining housing seems to be glued together, which prevents further
non-destructive analysis. There must be a mechanical relais hidden
in the plug, responsible for the noticeable *click* when turning it on
or off. Also, according to the magazine c’t, it uses a RN8209D chip
for measurements.

Firmware
--------

The meter’s firmware is updateable through the smartphone app. It is
fetched via the URL ``https://upgrade.revogi.com/?SN=<sn>``, where *sn*
is a hex-encoded string that can be read from the GATT endpoint
``0000fff1-​0000-​1000-​8000-​00805f9b34fb`` and contains an identifier,
as well as software and hardware revision. For hardware revision 3
devices are currently shipped with firmware version v1.16. That version
is not available for download, but v1.17_ is part of the Android app as
``bk3432_​ble_​app_​app22.bin``. v1.18_ is available on the update servers,
but currently not advertised for updates yet.

.. _v1.17: firmware/voltcraft-sem6000-v3-1.17.bin
.. _v1.18: firmware/voltcraft-sem6000-v3-1.18.bin

The downloadable firmware image has a 16 byte header followed by the
interrupt vector.

.. code-block:: c

	typedef struct {
		uint32_t crc32;
		uint16_t version;
		uint16_t length; /* Image length in 4 byte blocks */
		uint32_t uid;
		uint8_t  unknown1, unknown2;
		uint16_t unknown3;
	} imageHeader_t;

It looks like it is mapped at address ``0x0001b020``. Most of the code
is run in ARM Thumb mode.

A `high-level datasheet`_ for the BK3432 as well as a (semi-public) SDK_
are floating around the internet. According to that SDK the firmware
runs the proprietary `RivieraWaves Bluetooth`_ real-time operating
system (RTOS). Certain parts of this OS are not upgradeable over-the-air
(OTA), including the kernel and the Bluetooth stack itself. Thus the
OTA image above only contains the application-specific code of the
spb012ble.

.. _RivieraWaves Bluetooth: https://www.ceva-dsp.com/product/rivierawaves-bluetooth-platforms/
.. _high-level datasheet: https://developer.tuya.com/en/docs/iot/bk3432-datasheet?id=K9wmepyr04ob1
.. _SDK: https://gitee.com/shenzhen_bo_core/BK3432_BLE_SDK_public

The RN8209D is connected to the microcontroller through the UART2
interface at 9600 baud while UART1 at 115200 baud seems to be used
for debugging messages. Every 500 ms the firmware polls the following
registers in order:

1. *PowerPA (Active Power A)*.
   The average power over some unknown period of time. It is updated
   every 290 ms. Negative values are simply inverted. If the measured
   value changes by less than 51 mW the old value is used. A value
   less than 500 mW is turned into zero.
2. *URms (Voltage RMS)*.
3. *IARms (Current A RMS)*.
   Similarly to the power measurements, changes less than 20 mA will
   result in the old value being used. A value less than 20 mA is
   reported as zero.
4. *UFreq (Voltage Frequency)*. Updated only every 640 ms.

Power, voltage and current can be calibrated via the command 240 while the
UFreq register value is scaled interally by the constant ``0x6d3d3``, as
described by the RN8209D datasheet on page 25.

.. math::

	\begin{aligned}
	P &= \left \lfloor \frac{\textrm{PowerPA} \cdot P_{calib}}{1000} \right \rfloor \\
	U &= \left \lfloor \frac{\textrm{URms}}{U_{calib}} \right \rfloor \\
	I &= \left \lfloor \frac{\textrm{IARms} \cdot I_{calib}}{1000} \right \rfloor \\
	f &= \left \lfloor \frac{\left \lfloor \frac{3579545}{8} \right \rfloor}{\textrm{UFreq}} \right \rfloor
	\end{aligned}

Bugs
^^^^

Accumulation accuracy
	The current power readout is accumulated every five seconds,
	which is rather slow for quickly-changing loads like PC power
	supplies. This value is truncated to 1 W resolution for hourly
	energy statistics without proper rounding and propagated from
	there to daily and monthly statistics.

	.. code:: c

		/* Runs every five seconds */
		powerSum += power;
		if (minute == 59 && second == 55) {
		  energy += powerSum / (SECONDS_PER_HOUR / FIVE_SECONDS) / 1000;
		  update_energy_history_24h();
		  powerSum = 0;

	This can result in significant under-reporting of energy use for
	devices with low power draw of a few watts.
Input checks
	Inputs checks in the firmware are basically nonexistent. For
	instance the Android app allows setting power limits from 1000 W
	to 4000 W, while the firmware happily accepts any value from
	0 W to 65535 W.

	.. code:: c

		globalPowerLimit = (packet[4] << 8) | packet[5];
		status_reply(5, 0);
		flash_settings();

	The same applies to the device name, which overwrites arbitrary memory if too long:

	.. code:: c

		memset(globalDeviceNameA, 20);
		memcpy(globalDeviceNameA, &packet[4], packet[1]-3);
		memset(globalDeviceNameB, 20);
		memcpy(globalDeviceNameB, &packet[4], packet[1]-3);
		globalDeviceNameBLen = 20;
		status_reply(2, 0);
		flash_settings();
Scheduler rules soft-brick
	Under unknown circumstances the firmware crashes when adding or
	removing scheduler rules. The device will enter a soft-bricked
	state and not advertise any GATT services via BLE, making it
	impossible to control it remotely. Power-cycling only temporarily
	fixes the issue. Thus a full device reset is required to restore
	normal operation.
Authentication
	The client unlocks the meter using a four-digit PIN code. If that
	client – for whatever reason – disconnects the meter will
	advertise itself again over BLE and any other client can access
	the device without providing the PIN code, since it is already
	unlocked.

	Brute-forcing four digits takes about 30 minutes worst-case at
	a measured average rate of 5.7 tries per second.

	But even worse: The PIN can be reset to the default *without
	prior authentication* due to how commands are dispatched:

	.. code:: c

		bool checksum_correct = verify_checksum(packet);
		if (checksum_correct == false) {
		  return 0;
		}
		uint8_t action = packet[2];
		if (action == 23) {
		  handle_authentication(&packet[4]);
		} else if (action == 240) {
		  handle_calibration(&packet[4]);
		} else if (action == 48) {
		  handle_set_mac(&packet[4]);
		}
		if (is_authenticated != true) {
		  return 0;
		}
		if (action == 12) {

Switch behavior
	The physical switch on the top of the device is not debounced
	properly through hard- or software. Pressing it often results
	in the device switching off and on again or vice versa.
Default device name
	While purely cosmetic, the default device name is “Volcraft”,
	lacking a *t*.

Related work
------------

* `expect-based remote control <https://github.com/Heckie75/voltcraft-sem-6000>`__
  and `Bluetooth API documentation <https://github.com/Heckie75/voltcraft-sem-6000/blob/master/API.md>`__
* `Python library sem6000 <https://git.geekify.de/sqozz/sem6000>`__
* `Homeassistant integration <https://gitlab.youmi-lausanne.ch/amasson/hass-voltcraft-sem6000>`__
* `Web API <https://github.com/Matthias-pixel/voltcraft-api>`__
* `heise online: Acht Energiekosten-Messgeräte für den Hausgebrauch <https://www.heise.de/tests/Acht-Energiekosten-Messgeraete-fuer-den-Hausgebrauch-4614351.html?seite=all>`__ (probably for hardware revision 2)
* `c’t: Energiewaagen (issue 17/22, page 98) <https://www.heise.de/select/ct/2022/17/2215208081563964817>`__

