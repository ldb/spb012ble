# Copyright (c) 2022
# 	Lars-Dominik Braun <lars@6xq.net>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import sys, json
from datetime import datetime, timedelta
from collections import defaultdict

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

def readData (fd):
	for l in fd:
		o = json.loads (l)
		if 'time' in o:
			o['time'] = datetime.fromisoformat (o['time'])
		yield o

def main ():
    xtime = []
    power = []
    powerFactor = []
    voltage = []
    current = []
    hourlyEnergy = defaultdict (list)
    #hourlyEnergyMeter = defaultdict (list)
    prevMeasurement = None
    #prevHourlyEnergyMeter = None

    # For total/average measurements
    totalEnergy = 0
    totalTime = 0

    for packet in readData (sys.stdin):
    	if packet['kind'] == 'measureNow':
    		time = packet['time']
    		xtime.append (time)
    		power.append (packet['power']/1000)

    		try:
    			# XXX: dirty hack
    			powerFactor.append (min (1.0, packet['power']/(packet['voltage']*packet['current'])))
    		except ZeroDivisionError:
    			powerFactor.append (np.nan)

    		if prevMeasurement is not None:
    			# Integrate total energy [kWh]
    			t = (time-prevMeasurement).total_seconds ()/60/60
    			p = packet['power']/1000/1000
    			# Round to the current hour
    			hourlyEnergy[time.replace (minute=0, second=0, microsecond=0)].append (p*t)

    			totalEnergy += (p*1000)*t
    			totalTime += t

    		# Our integrated total energy is pretty close to the meter’s calculations, so I think we don’t need it.
    		#if prevHourlyEnergyMeter is not None and packet['totalEnergy'] > prevHourlyEnergyMeter:
    		#	t = time.replace (minute=0, second=0, microsecond=0)
    		#	hourlyEnergyMeter[t] = (packet['totalEnergy']-prevHourlyEnergyMeter)/1000

    		voltage.append (packet['voltage'])
    		current.append (packet['current']/1000)

    		prevMeasurement = time
    		#prevHourlyEnergyMeter = packet['totalEnergy']

    xhourly = []
    yhourlyEnergy = []
    for k, v in sorted (hourlyEnergy.items (), key=lambda x: x[0]):
    	xhourly.append (k)
    	yhourlyEnergy.append (sum (v))
    print (xhourly, yhourlyEnergy)
    perKwh = 40
    averagePower = totalEnergy/totalTime
    print (f'total kWh {totalEnergy:1.6f} Wh @ {perKwh} cents/kWh {(totalEnergy/1000*perKwh)/100:1.4f}€, average power {averagePower:1.6f} W')

    with plt.style.context('Solarize_Light2'):
    	fig, (ax1, ax3, ax5) = plt.subplots(3, figsize=(8,9), sharex=True)

    	ax2 = ax1.twinx()
    	ax1.plot (xtime, power, color=f'C1', linewidth=0.7)
    	ax2.plot (xtime, powerFactor, color=f'C2', linewidth=0.7)
    	ax1.axhline (averagePower, linewidth=0.7, color='C3')

    	ax1.set_ylabel ('Leistung [W]')
    	ax2.set_ylabel ('Leistungsfaktor')
    	ax2.grid (False)

    	ax4 = ax3.twinx()
    	ax3.plot (xtime, voltage, color=f'C1', linewidth=0.7)
    	ax4.plot (xtime, current, color=f'C2', linewidth=0.7)

    	ax3.set_ylabel ('Spannung [V]')
    	ax4.set_ylabel ('Strom [A]')
    	ax4.grid (False)

    	# XXX: use individual patches, so start/end is correct.
    	ax5.bar (xhourly, yhourlyEnergy, color='C1', align='edge', width=timedelta (minutes=45))
    	ax5.set_ylabel ('Verbrauch [kWh]')

    	ax5.xaxis_date ()
    	ax5.xaxis.set_major_formatter(
    		mdates.ConciseDateFormatter(ax5.xaxis.get_major_locator()))

    	plt.savefig ('energy.png', dpi=300)

if __name__ == '__main__':
	main ()

