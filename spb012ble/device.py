# Copyright (c) 2022
# 	Lars-Dominik Braun <lars@6xq.net>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""
Device abstraction

We could also use https://github.com/hbldh/bleak instead of talking to
bluez via DBus directly, but that seemed overkill.
"""

import logging, json, asyncio, struct
from enum import IntEnum, IntFlag
from datetime import date, datetime, timezone, timedelta, time
from dateutil.relativedelta import relativedelta

import aiohttp

logger = logging.getLogger (__name__)

BLUEZ_SERVICE = 'org.bluez'
BLUEZ_DEV_IFACE = 'org.bluez.Device1'
BLUEZ_CHR_IFACE = 'org.bluez.GattCharacteristic1'

def bytesToInt (b):
	""" Convert big endian bytes of arbitrary length to integer """
	i = 0
	b = list (b)
	b.reverse ()
	for (j, c) in enumerate (b):
		i += c << (8*j)
	return i

def bytesToHex (b):
	""" Convert bytes b to hex string """
	return ''.join (map (lambda v: f'{v:02x}', list (b)))

def asciiToBytes (s):
	""" Convert ASCII numbers to bytes, i.e. '0' → b'\x00' """
	return [ord(x)-ord('0') for x in s]

class Command(IntEnum):
	SET_TIME = 1
	SET_NAME = 2
	SET_MODE = 3
	GET_MEASUREMENT = 4
	SET_POWER_PROTECTION = 5
	SET_TIMER = 8
	GET_TIMER = 9
	GET_HISTORY_24H = 10
	GET_HISTORY_30D = 11
	GET_HISTORY_12M = 12
	SET_CONFIG = 15
	GET_SETTINGS = 16
	SET_SCHEDULER = 19
	GET_SCHEDULER = 20
	# Authenticate or set password
	AUTHENTICATION = 23
	GET_NAME = 24
	CALIBRATE = 240
	UPDATE_FIRMWARE = 255

class Mode (IntEnum):
	""" Device mode for Command.SET_MODE """
	OFF = 0
	ON = 1
	UNKNOWN1 = 2 # unknown, red blinking, no response
	ON_NO_LED = 3 # looks like Config.LED off

class Config (IntEnum):
	""" Device configuration register for Command.SET_CONFIG """
	RESET = 0
	NIGHT_MODE = 1
	CLEAR_DATA = 2
	ICON = 3
	PRICE = 4
	LED = 5

class TimerMode (IntEnum):
	RESET = 0
	TURN_ON = 1
	TURN_OFF = 2

class SchedulerAction (IntEnum):
	TURN_OFF = 0
	TURN_ON = 1

class SchedulerDay (IntFlag):
	SUNDAY = 1
	MONDAY = 2
	TUESDAY = 4
	WEDNESDAY = 8
	THURSDAY = 16
	FRIDAY = 32
	SATURDAY = 64

class SchedulerSetMode (IntEnum):
	ADD = 0
	EDIT = 1
	REMOVE = 2

class FirmwareUpdateMode (IntEnum):
	CANCEL = 0
	START = 1
	FINISH = 2

class VoltcraftSEM6000Exception (Exception):
	pass

class DeviceNotFound (VoltcraftSEM6000Exception):
	""" The BLE device was not found """
	pass

class VoltcraftSEM6000:
	defaultPin = '0000'

	def __init__ (self, bus, address):
		self.bus = bus
		self.recvQueue = asyncio.Queue ()
		self.requestLock = asyncio.Lock ()
		self.address = address
		# The default
		self.pin = self.defaultPin

	async def connect (self):
		bus = self.bus

		devices = await self.findDevice (bus)
		if self.address not in devices:
			raise DeviceNotFound ()
		(devicePath, device) = devices[self.address]

		connected = await device.get_connected ()
		logger.debug (f'Connected to device: {connected}')
		if not connected:
			logger.debug ('Connecting to device')
			await device.call_connect ()
			# Connecting to specific profile does not seem to work.

		logger.debug ('Waiting for services to be available')
		while True:
			resolved = await device.get_services_resolved ()
			if resolved:
				break
			await asyncio.sleep (0.1)

		logger.debug ('Connecting to services')
		gatt = await self.findGatt (bus, devicePath)
		try:
			_, _, self.upgrade = gatt['0000fff1-0000-1000-8000-00805f9b34fb']
			_, _, self.write = gatt['0000fff3-0000-1000-8000-00805f9b34fb']
			notifyPath, proxy, self.notify = gatt['0000fff4-0000-1000-8000-00805f9b34fb']
		except KeyError as e:
			logger.error (f'GATT service {e.args[0]} not found')
			raise DeviceNotFound ()
		self.notifyProps = proxy.get_interface ('org.freedesktop.DBus.Properties')

		# Enable notifications
		logger.debug ('Enabling notifications')
		await self.notify.call_start_notify ()
		self.notifyProps.on_properties_changed (self.onNotify)

	async def onNotify(self, interface_name, changed_properties, invalidated_properties):
		""" Notification handler, which receives responses for requests """

		now = datetime.now (timezone.utc) 
		for changed, variant in changed_properties.items():
			packet = variant.value

			logger.debug (f'→ {bytesToHex (packet)}')

			if not packet[0] == 0xf:
				logger.debug (f'Expected framing 0xf at the beginning of {packet}')
				continue

			# Cut away framing
			length = packet[1]
			packet = packet[2:length+2]

			checksum = packet[-1]
			checksumComputed = (1+sum (list (packet[0:-1])))%256
			# The checksum is regularly wrong.
			#assert checksum == checksumComputed, (checksum, checksumComputed, packet)

			command = packet[0]

			payload = packet[2:-1]

			await self.recvQueue.put ((now, command, payload))

	@staticmethod
	async def dbusChildren (bus, path):
		introspection = await bus.introspect(BLUEZ_SERVICE, path)
		for n in introspection.nodes:
			yield f'{path}/{n.name}'

	async def findDevice (self, bus):
		""" Find device with specific address on all adapters """

		results = dict ()

		# XXX: do not hard-code prefix
		async for adapterPath in self.dbusChildren (bus, '/org/bluez'):
			async for devicePath in self.dbusChildren (bus, adapterPath):
				introspection = await bus.introspect (BLUEZ_SERVICE, devicePath)
				proxy = bus.get_proxy_object (BLUEZ_SERVICE, devicePath, introspection)
				interface = proxy.get_interface (BLUEZ_DEV_IFACE)
				devaddress = await interface.get_address ()
				results[devaddress] = (devicePath, interface)
		return results

	async def findGatt (self, bus, devicePath):
		"""
		Get all GATT endpoints for devicePath and return
		them in a dict of (uuid, (path, proxy, iface))
		"""
		results = dict ()

		async for servicePath in self.dbusChildren (bus, devicePath):
			async for gattPath in self.dbusChildren (bus, servicePath):
				introspection = await bus.introspect(BLUEZ_SERVICE, gattPath)
				proxy = bus.get_proxy_object (BLUEZ_SERVICE, gattPath, introspection)
				iface = proxy.get_interface (BLUEZ_CHR_IFACE)
				uuid = await iface.get_uuid ()

				results[uuid] = (gattPath, proxy, iface)

		return results

	async def request (self, command, payload=None, timeout=10):
		payload = payload or []
		# 1 byte command, 1 byte padding and 1 byte checksum
		length = 2+len (payload)+1
		payload = [int (command), 0x00] + list (payload)
		checksum = (1+sum (payload))%256
		packet = [0x0f, length] + payload + [checksum, 0xff, 0xff]
		packet = bytes (packet)

		logger.debug (f'← {bytesToHex (packet)}')

		# Make sure that no-one ever writes without getting its reply first
		async with self.requestLock:
			while True:
				await self.write.call_write_value (packet, {})
				# Most commands should not take this long,
				# so it’s fine to wrap in a timeout. It
				# usually means the device has gone away.
				stamp, notifyCommand, notifyPayload = \
						await asyncio.wait_for (self.recvQueue.get (), timeout=timeout)
				if command == notifyCommand:
					break
				# Drop notifications we did not ask for.

		return stamp, notifyPayload

	async def getDeviceInfo (self):
		data = await self.upgrade.call_read_value ({})
		ident = data[0:7]
		firmwareVersion = (data[11], data[12])
		# Looks like this one is used for different power limits (on: 1000–2600, off: 1000–4000)
		swissBool = data[8] == 3
		hardwareRevision = data[13]
		return ident, firmwareVersion, swissBool, hardwareRevision

	async def checkUpdate (self):
		data = await self.upgrade.call_read_value ({})
		# XXX: Don’t know the checksum algorithm yet.
		#checksum = sum (list (data[:-1]))%256
		#assert data[-1] == checksum, (data, checksum)
		logger.debug (f'Checking updates for SN={data.hex()}')
		async with aiohttp.ClientSession() as session:
			async with session.get ('https://upgrade.revogi.com/', params=dict(SN=data.hex ())) as resp:
				# Cannot use resp.json(), because the response does not have the correct mimetype.
				return json.loads (await resp.text ()), resp.url

	async def authenticate (self, pin):
		self.pin = pin
		payload = [0] + asciiToBytes (pin) + (4*[0])
		stamp, resp = await self.request (Command.AUTHENTICATION, payload)
		return resp[0] == 0

	async def setPin (self, newpin=None):
		""" Set or reset pin (if newpin is None) """

		if newpin is None:
			mode = 2
			newpin = '0000'
		else:
			mode = 1
		payload = [mode] + asciiToBytes (newpin) + asciiToBytes (self.pin)
		stamp, resp = await self.request (Command.AUTHENTICATION, payload)
		ret = resp[0] == 0
		if ret:
			self.pin = newpin
		return ret

	async def sync (self):
		# Use local time.
		now = datetime.now ()
		payload = [now.second, now.minute, now.hour, now.day, now.month, now.year//256, now.year%256, 0, 0]
		stamp, resp = await self.request (Command.SET_TIME, payload)
		# No return value in response
		return now

	async def measure (self):
		# These bytes seem to be actually unused.
		stamp, resp = await self.request (Command.GET_MEASUREMENT)
		onOff = bool (resp[0])
		power = bytesToInt (resp[1:4])
		voltage = resp[4]
		current = bytesToInt (resp[5:7])
		frequency = resp[7]
		powerFactor = bytesToInt (resp[8:10])
		# Seems to be updated every hour at :59:55 based on internal RTC clock)
		totalEnergy = bytesToInt (resp[10:14])
		return (stamp, onOff, power, voltage, current, frequency, powerFactor, totalEnergy)

	async def getSettings (self):
		stamp, resp = await self.request (Command.GET_SETTINGS)
		valleyActive = bool (resp[0])
		ordinaryPrice = resp[1]
		valleyPrice = resp[2]
		valleyStart = bytesToInt (resp[3:5])
		valleyStart = time (hour=valleyStart//60, minute=valleyStart%60)
		valleyEnd = bytesToInt (resp[5:7])
		valleyEnd = time (hour=valleyEnd//60, minute=valleyEnd%60)
		ledEnabled = bool (resp[7])
		iconName = resp[8]
		powerProtect = bytesToInt (resp[9:11])
		return valleyActive, ordinaryPrice, valleyPrice, valleyStart, valleyEnd, ledEnabled, iconName, powerProtect

	async def setMode (self, mode):
		assert isinstance (mode, Mode)
		stamp, resp = await self.request (Command.SET_MODE, [int (mode)])
		# There is no indication whether a command succeeded
		# or not. Even nonexistent ones return 0 in resp[0]
		return True

	async def getName (self):
		stamp, resp = await self.request (Command.GET_NAME)
		# The packet length is off by two.
		resp = resp[:-2]
		return resp.rstrip (b'\0').decode ('utf-8', errors='replace')

	async def setName (self, name):
		payload = name.encode ('utf-8')
		stamp,resp = await self.request (Command.SET_NAME, payload)

	async def setConfig (self, key : Config, payload : bytes):
		payload = [int (key)] + payload
		stamp, resp = await self.request (Command.SET_CONFIG, payload)
		# No response status

	async def setLed (self, on : bool):
		# 0 is off, everything else is on.
		return await self.setConfig (Config.LED, [int (on)])

	async def setIcon (self, i : int):
		return await self.setConfig (Config.ICON, [i])

	async def clearHistory (self):
		""" Clears totalEnergy and yearly, daily, hourly history data """
		return await self.setConfig (Config.CLEAR_DATA, [])

	async def reset (self):
		""" Resets and disconnects the meter """
		return await self.setConfig (Config.RESET, [])

	async def setPrice (self, ordinary, valley):
		"""
		Price calculation happens in the app, the device only
		returns a percentage of valley wattage used in the
		4th byte
		"""
		return await self.setConfig (Config.PRICE, [ordinary, valley])

	async def setValley (self, onoff: bool, start: time, end: time):
		startMinutes = start.hour*60+end.minute
		endMinutes = end.hour*60+end.minute
		payload = [1 if onoff else 0, startMinutes//256, startMinutes%256, endMinutes//256, endMinutes%256]
		return await self.setConfig (Config.NIGHT_MODE, payload)

	async def setPowerProtection (self, v):
		"""
		Set power protction limit v in Watts. If the device
		consumes more power it’ll be switched off and the red
		light blinks.
		"""
		payload = [v//256, v%256]
		stamp, resp = await self.request (Command.SET_POWER_PROTECTION, payload)
		# No response status.

	async def setTimer (self, mode: TimerMode, date: datetime):
		if date.year-2000 >= 256:
			raise ValueError (f'Date {date} too far in the future.')
		if date.year < 2000:
			raise ValueError (f'Date {date} too far in the past.')
		payload = [int (mode), date.second, date.minute, date.hour, date.day, date.month, date.year-2000, 0, 0, 0, 0]
		stamp, resp = await self.request (Command.SET_TIMER, payload)
		return resp[0] == 0

	async def getTimer (self):
		stamp, resp = await self.request (Command.GET_TIMER)
		try:
			action = TimerMode (resp[0])
		except ValueError:
			action = resp[0]
		try:
			expiry = datetime (resp[6]+2000, resp[5], resp[4], resp[3], resp[2], resp[1])
		except ValueError:
			expiry = None
		total = timedelta (seconds=bytesToInt (resp[7:10]))
		return action, expiry, total

	async def getScheduler (self):
		payload = [0, 0, 0]
		stamp, resp = await self.request (Command.GET_SCHEDULER, payload)

		size = 12
		count = resp[0]

		scheduler = dict ()
		for i in range (count):
			j = 1+i*size
			chunk = resp[j:j+size]
			slot = chunk[0]
			active = chunk[1] == 1
			try:
				action = SchedulerAction (chunk[2])
			except ValueError:
				action = chunk[2]
			weekday = SchedulerDay (chunk[3])
			year = chunk[4]
			month = chunk[5]
			day = chunk[6]
			hour = chunk[7]
			minute = chunk[8]
			# The date may be invalid for weekday-based schedulers.
			if year != 0 and month != 0 and day != 0:
				date = datetime (year+2000, month, day, hour, minute)
			elif weekday != 0:
				date = time (hour, minute)
			else:
				date = None
			scheduler[slot] = (active, action, weekday, date)

		return scheduler

	async def removeScheduler (self, slot: int):
		payload = [int (SchedulerSetMode.REMOVE), slot] + 10*[0]
		stamp, resp = await self.request (Command.SET_SCHEDULER, payload)
		return resp[0] == 0

	async def editScheduler (self, slot: int, active: bool, action: SchedulerAction, weekday: SchedulerDay, date: datetime):
		payload = [int (SchedulerSetMode.EDIT), slot, 1 if active else 0, int (action), int (weekday), date.year-2000, date.month, date.day, date.hour, date.minute, 0, 0]
		stamp, resp = await self.request (Command.SET_SCHEDULER, payload)
		return resp[0] == 0

	async def addScheduler (self, active: bool, action: SchedulerAction, weekday: SchedulerDay, date: datetime):
		payload = [int (SchedulerSetMode.ADD), 0, 1 if active else 0, int (action), int (weekday), date.year-2000, date.month, date.day, date.hour, date.minute, 0, 0]
		stamp, resp = await self.request (Command.SET_SCHEDULER, payload)
		return resp[0] == 0

	async def getHistory24H (self):
		stamp, resp = await self.request (Command.GET_HISTORY_24H)

		data = dict ()
		now = datetime.now ()
		base = now.replace (minute=0, second=0, microsecond=0)
		for i in range (24):
			data[base-timedelta (hours=24-1-i)] = bytesToInt (resp[i*2:i*2+2])
		return data

	async def getHistory30D (self):
		stamp, resp = await self.request (Command.GET_HISTORY_30D)

		data = dict ()
		base = date.today ()
		for i in range (30):
			j = i*4
			data[base-timedelta (days=30-1-i)] = (bytesToInt (resp[j:j+3]), resp[j+3])
		return data

	async def getHistory12M (self):
		stamp, resp = await self.request (Command.GET_HISTORY_12M)

		data = dict ()
		now = date.today ()
		base = now.replace (day=1)

		for i in range (12):
			j = i*4
			data[base-relativedelta (months=12-1-i)] = (bytesToInt (resp[j:j+3]), resp[j+3])
		return data

	async def calibrate (self, power, voltage, current):
		# Only works if the raw power value is greater than 100.
		powerRaw = stuct.pack ('>I', power)
		currentRaw = stuct.pack ('>H', current)
		stamp, resp = await self.request (Command.CALIBRATE, [0] + list (powerRaw) + list (currentRaw) + [voltage])
		# No failure possible.
		return True

	async def updateFirmware (self, data):
		crcExpected, version, blocks, uid, crcStatus, sectionStatus, romVersion = struct.unpack ('<IHHIBBH', data[:16])
		length = blocks*4

		if length != len (data):
			raise Exception (f'Data length {len (data)} does not match expected length {length}')

		# binascii.crc32 does not produce the correct crc32,
		# so their implementation must be slightly different.
		crc32Table = dict ()
		for i in range (256):
			c = i
			for bit in range (8):
				if c&1:
					c = (c>>1)^0xEDB88320
				else:
					c = c>>1
			crc32Table[i] = c

		crc = 0xffffffff
		# Header is not included in CRC32
		for c in data[16:]:
			crc = (crc>>8) ^ (crc32Table[(crc^c)&0xff])
		if crc != crcExpected:
			raise Exception (f'CRC32 ({crc:08x}) does not match expected value {crcExpected:08x}')

		blockSize = 128
		stamp, resp = await self.request (Command.UPDATE_FIRMWARE, [FirmwareUpdateMode.START])

		try:
			offset = 0
			yield dict (length=length, offset=0)
			while offset < length:
				buf = data[offset:offset+blockSize]

				await self.upgrade.call_write_value (buf, {})

				offset += len (buf)
				yield dict (length=length, offset=offset)
		except (Exception, asyncio.CancelledError):
			stamp, resp = await self.request (Command.UPDATE_FIRMWARE, [FirmwareUpdateMode.CANCEL])
			raise
		finally:
			# The Android client also sends a checksum, but it is unused.
			stamp, resp = await self.request (Command.UPDATE_FIRMWARE, [FirmwareUpdateMode.FINISH], timeout=120)

