# Copyright (c) 2022
# 	Lars-Dominik Braun <lars@6xq.net>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import asyncio, logging, json, sys, inspect, re, math
from enum import IntEnum, IntFlag, Enum
from datetime import datetime, timezone, timedelta, time
from operator import attrgetter
from collections import OrderedDict

from dbus_next.aio import MessageBus
from dbus_next import BusType

from .device import VoltcraftSEM6000, Mode, TimerMode, DeviceNotFound, SchedulerAction, SchedulerDay

logger = logging.getLogger (__name__)

class Encoder (json.JSONEncoder):
	def default (self, o):
		if isinstance (o, datetime) or isinstance (o, time):
			return o.isoformat ()
		if isinstance (o, timedelta):
			return o.total_seconds ()
		elif isinstance (o, bytes):
			return o.hex ()
		elif isinstance (o, IntFlag):
			return repr (o)
		elif getattr (o, '__str__'):
			return str (o)

		return super ().default (o)

def log (kind, **kwargs):
	kwargs['kind'] = kind
	json.dump (kwargs, sys.stdout, cls=Encoder)
	sys.stdout.write ('\n')
	sys.stdout.flush ()

actions = OrderedDict ()

def action (f):
	"""
	Decorator, adding f to the list of available commands with “do” prefix removed.
	"""
	name = f.__name__[2:]
	name = name[0].lower () + name[1:]
	actions[name] = f

	return f

def getParams (f):
	params = inspect.signature (f).parameters
	return filter (lambda x: x.annotation not in (MessageBus, VoltcraftSEM6000), params.values ())

@action
async def doHelp (bus: MessageBus, meter: VoltcraftSEM6000):
	""" List available commands """
	print ('Usage: command.py [name:param1,param1] …\n')
	for name, f in actions.items ():
		params = getParams (f)
		params = ','.join (map (lambda x: x.name.upper(), params))
		s = f'  {name}'
		if params:
			s += ':' + params
		if f.__doc__:
			if len (s) >= 30:
				s += '\n' + 30*' '
			else:
				s = s.ljust (30)
			doc = f.__doc__
			doc = re.sub (r'(^\s*|\s*$)', '', doc)
			doc = re.sub (r'\n\s*', '\n' + 32*' ', doc)
			s += doc
		print (s)

class Address:
	def __new__ (self, s):
		octets = [int (x, 16) for x in s.split (':')]
		if len (octets) == 6:
			return s
		raise ValueError (f'{s} is not a valid Bluetooth device address')

@action
async def doConnect (bus: MessageBus, meter: VoltcraftSEM6000, address: Address):
	""" Connect to device """
	logger.info (f'Connecting to {address}')
	meter = VoltcraftSEM6000 (bus, address)
	try:
		await meter.connect ()
	except DeviceNotFound:
		logger.error (f'Device {address} not found')
		return 1
	return meter

class Pin:
	def __new__ (obj, s):
		if len (s) == 4 and all (map (lambda x: x.isdigit (), s)):
			return s
		else:
			raise ValueError (f'{s} is not a valid pin of four digits')

@action
async def doAuth (bus: MessageBus, meter: VoltcraftSEM6000, pin: Pin):
	""" Authenticate to device with four-digit PIN """
	logger.info ('Authenticating to device')
	success = await meter.authenticate (pin)
	if not success:
		logger.error ('Pin was wrong')
		return False
	return dict (result=success)

@action
async def doAuthBruteforce (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Brute-force the device PIN """
	loop = asyncio.get_running_loop ()
	start = loop.time ()
	for i in range (1, 10000):
		pin = f'{i:04d}'
		now = loop.time ()
		logger.info (f'Trying PIN {pin}, {i/(now-start):.1f} tries/s')
		success = await meter.authenticate (pin)
		if success:
			break
	return dict (result=success, pin=pin)

@action
async def doSetPin (bus: MessageBus, meter: VoltcraftSEM6000, newPin: Pin):
	""" Change device PIN """
	result = await meter.setPin (newPin)
	return dict (result=result, pin=newPin)

@action
async def doResetPin (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Reset device PIN to default """
	result = await meter.setPin (None)
	return dict (result=result, pin=meter.defaultPin)

@action
async def doSync (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Synchronize date and time """
	logger.info ('Synchronizing time and date')
	now = await meter.sync ()
	return dict (now=now)

@action
async def doMeasure (bus: MessageBus, meter: VoltcraftSEM6000):
	"""
	Measure current power

	As well as voltage, current, frequency and total energy.
	"""
	loop = asyncio.get_event_loop ()
	while True:
		try:
			stamp, onOff, power, voltage, current, frequency, powerFactor, totalEnergy = await meter.measure ()
			log ('measureNow',
					time=stamp,
					on=onOff,
					power=power,
					voltage=voltage,
					current=current,
					frequency=frequency,
					powerFactor=powerFactor,
					totalEnergy=totalEnergy,
					)
		except asyncio.TimeoutError:
			pass

		# It looks like the meter updates values every 500 ms.
		# Using absolute time instead of duration (end-start) avoids drift.
		now = loop.time ()
		await asyncio.sleep (0.5-(now%0.5))

@action
async def doGetHistory (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Get historic energy consumption """
	def transformDictKey (d):
		return dict (map (lambda x: (x[0].isoformat(), x[1]), d.items ()))

	hourly = transformDictKey (await meter.getHistory24H ())
	daily = transformDictKey (await meter.getHistory30D ())
	monthly = transformDictKey (await meter.getHistory12M ())
	return dict (hourly=hourly, daily=daily, monthly=monthly)

@action
async def doGetSettings (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Fetch device settings """
	logger.info ('Fetching current settings')
	valleyActive, ordinaryPrice, valleyPrice, valleyStart, valleyEnd, ledEnabled, iconName, powerProtect = await meter.getSettings ()
	return dict (valleyActive=valleyActive, ordinaryPrice=ordinaryPrice, valleyPrice=valleyPrice, valleyStart=valleyStart, valleyEnd=valleyEnd, ledEnabled=ledEnabled, iconName=iconName, powerProtect=powerProtect)

@action
async def doGetName (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Get device’s Bluetooth name """
	name = await meter.getName ()
	return dict (name=name)

@action
async def doSetName (bus: MessageBus, meter: VoltcraftSEM6000, name: str):
	""" Set device’s Bluetooth name """
	result = await meter.setName (name)
	return dict (result=result, name=name)

@action
async def doSetPowerLimit (bus: MessageBus, meter: VoltcraftSEM6000, limit: int):
	"""
	Set power limit

	The limit is enforced by the firmware.
	"""
	result = await meter.setPowerProtection (limit)
	return dict (result=result, limit=limit)

class Bool:
	def __new__ (obj, s):
		if s in ('1', 'on'):
			return True
		elif s in ('0', 'off'):
			return False
		else:
			raise ValueError (f'{s} is not a valid boolean')

@action
async def doSwitch (bus: MessageBus, meter: VoltcraftSEM6000, on: Bool):
	""" Turn the meter on/off """
	result = await meter.setMode (Mode.ON if on else Mode.OFF)
	return dict (result=result)

@action
async def doLed (bus: MessageBus, meter: VoltcraftSEM6000, on: Bool):
	""" Turn the LED on/off """
	result = await meter.setLed (on)
	return dict (result=result)

@action
async def doSetPrice (bus: MessageBus, meter: VoltcraftSEM6000, normal: int, valley: int):
	""" Set energy price per kWh """
	result = await meter.setPrice (normal, valley)
	return dict (result=result)

class ParsedTime:
	def __new__ (self, s):
		return time.fromisoformat (s)

@action
async def doSetValley (bus: MessageBus, meter: VoltcraftSEM6000, on: Bool, start: ParsedTime, end: ParsedTime):
	""" Set up low tariff mode """
	result = await meter.setValley (on, start, end)
	return dict (result=result)

@action
async def doGetTimer (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Get current timer status """
	action, expiry, total = await meter.getTimer ()
	return dict (action=action, expiry=expiry, total=total)

class ParsedDatetimeOrSeconds:
	def __new__ (self, s):
		try:
			return datetime.now () + timedelta (seconds=int (s))
		except ValueError:
			return datetime.fromisoformat (s)

@action
async def doSetTimer (bus: MessageBus, meter: VoltcraftSEM6000, mode: TimerMode, date: ParsedDatetimeOrSeconds):
	"""
	Set timer to turn the device on/off or disable timer

	MODE is RESET, TURN_OFF or TURN_ON.
	DATE can be an ISO format datetime or seconds.
	"""
	result = await meter.setTimer (mode, date)
	return dict (result=result)

def intFlagToList (flag):
	""" Turn flag into a list of names """
	return list (filter (lambda x: x is not None, map (lambda x: x.name if x in flag else None, list (type (flag)))))

@action
async def doGetScheduler (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Get all scheduler """
	result = await meter.getScheduler ()
	for slot in result.keys ():
		active, action, weekday, date = result[slot]
		# There is no easy way to support IntFlag in JSONEncoder, because it subclasses int.
		if isinstance (weekday, IntFlag):
			weekday = intFlagToList (weekday)
		result[slot] = dict (active=active, action=action.name, weekday=weekday, date=date)
	return dict (result=result)

class ParsedDateOrTime:
	def __new__ (self, s):
		try:
			return datetime.fromisoformat (s)
		except ValueError:
			t = time.fromisoformat (s)
			return datetime (2000, 1, 1, t.hour, t.minute)

@action
async def doAddScheduler (bus: MessageBus, meter: VoltcraftSEM6000, active: Bool, action: SchedulerAction, weekday: SchedulerDay, date: ParsedDateOrTime):
	"""
	Add a new scheduler

	ACTIVE can be on/off
	ACTION is either TURN_ON or TURN_OFF
	WEEKDAY can be SUNDAY…SATURDAY or a combination thereof,
	  i.e. MONDAY+TUESDAY
	"""
	result = await meter.addScheduler (active, action, weekday, date)
	return dict (result=result)

@action
async def doSetScheduler (bus: MessageBus, meter: VoltcraftSEM6000, slot: int, active: Bool, action: SchedulerAction, weekday: SchedulerDay, date: ParsedDateOrTime):
	""" Modify scheduler in SLOT """
	result = await meter.editScheduler (slot, active, action, weekday, date)
	return dict (result=result)

@action
async def doRemoveScheduler (bus: MessageBus, meter: VoltcraftSEM6000, slot: int):
	""" Remove scheduler SLOT """
	result = await meter.removeScheduler (slot)
	return dict (result=result)

@action
async def doClearHistory (bus: MessageBus, meter: VoltcraftSEM6000):
	"""
	Clear historic data

	Resets total energy consumption counter, as well as hourly,
	daily and monthly counters to zero.
	"""
	logger.info ('Clearing history')
	result = await meter.clearHistory ()
	return dict (result=result)

@action
async def doReset (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Reset meter to factory defaults """
	await meter.reset ()

@action
async def doDeviceInfo (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Fetch device firmware and hardware revision """
	ident, firmwareVersion, swissBool, hardwareRevision = await meter.getDeviceInfo ()
	return dict (ident=ident, firmwareVersion=firmwareVersion,
			swissBool=swissBool, hardwareRevision=hardwareRevision)

@action
async def doUpdateCheck (bus: MessageBus, meter: VoltcraftSEM6000):
	""" Check for firmware updates """
	result, url = await meter.checkUpdate ()
	return dict (result=result, url=url)

@action
async def doUpdateFirmware (bus: MessageBus, meter: VoltcraftSEM6000, path: str):
	loop = asyncio.get_running_loop ()

	with open (path, 'rb') as fd:
		start = loop.time ()
		g = meter.updateFirmware (fd.read ())
		async for status in g:
			offset = status['offset']
			length = status['length']
			pct = offset/length*100
			now = loop.time ()
			if offset > 0:
				remaining = int (math.ceil (((100-pct)*((now-start)/pct))/60))
			else:
				remaining = None
			logger.info (f'Firmware update wrote {offset} of {length} bytes ({pct:0.1f}%), {remaining or "–"} min remaining')

class FunctionNotAvailable (Exception):
	pass

def parseArgs (args):
	""" Parse commandline arguments and return list of functions to run plus their arguments """

	run = []
	for i, a in enumerate (args):
		try:
			name, strArgs = a.split (':', 1)
		except ValueError:
			name = a
			strArgs = ''
		if name not in actions:
			raise FunctionNotAvailable (name, i)
		f = actions[name]

		# Use the function signature’s type annotations to infer parser for arguments.
		params = getParams (f)
		fargs = {}
		for value, p in zip (strArgs.split (','), params):
			if issubclass (p.annotation, IntEnum):
				value = p.annotation[value.upper ()]
			elif issubclass (p.annotation, IntFlag):
				tmpValue = 0
				if len (value) > 0:
					for v in value.split ('+'):
						tmpValue |= p.annotation[v.upper()]
				value = tmpValue
			elif issubclass (p.annotation, Enum):
				value = p.annotation (int (value))
			else:
				value = p.annotation (value)
			fargs[p.name] = value

		run.append ((name, f, fargs))

	return run

async def _main (args):
	logging.basicConfig (level=logging.DEBUG)

	bus = await MessageBus (bus_type=BusType.SYSTEM).connect()
	meter = None

	try:
		run = parseArgs (args)
	except FunctionNotAvailable as e:
		logger.error (f'Function “{e.args[0]}” (argument {e.args[1]+1}) is not available.')
		await doHelp (bus, meter)
		return 1

	for name, f, fargs in run:
		result = await f (bus, meter, **fargs)
		if result is None:
			pass
		elif isinstance (result, dict):
			log (name, **result)
		elif isinstance (result, VoltcraftSEM6000):
			meter = result
		else:
			return 1

	return 0

def main ():
	try:
		sys.exit (asyncio.run (_main (sys.argv[1:])))
	except KeyboardInterrupt:
		sys.exit (0)

if __name__ == '__main__':
	main ()

